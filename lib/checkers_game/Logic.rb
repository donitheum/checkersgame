# Most of the games logic
module CheckersGame
  class Logic

    attr_reader :active_piece #Hold reference to the selected piece

    def initialize(board_instance)
      @board = board_instance
      @active_piece = nil
    end


    # Determines which  field is selected based on mouse coordinates return array coordinates
    def process_keypress(mouse_x, mouse_y)
      mouse_x = mouse_x - @board.top_pos_x
      mouse_y = mouse_y - @board.top_pos_y
      mouse_x = mouse_x / Board::FIELD_SIZE
      mouse_y = mouse_y / Board::FIELD_SIZE
      # puts "We are at #{mouse_x}, #{mouse_y}"
      # pos = PositionNames.get(mouse_x.floor,mouse_y.floor)
      # puts pos
      process_selection(mouse_x.floor,mouse_y.floor,@board.fields.last)
    end



    def process_selection(x,y,field)
      unless get_selected_value != nil && field_empty?(Point.new(x,y),field)
        @active_piece = field.get(x,y)
        points = adjacent_empty Point.new(x,y), field

      end
      #Are we gonna move the piece or just select one
      move_pos = nil
      #If we already have a piece selected and we want to move it than do it
      if get_selected_value != nil && field.available_moves !=nil
        field.available_moves.each do |v|
          if v.x == x && v.y == y
            move_pos = Point.new(x,y)
            break
          end
        end
      end

      # Move the piece to selected location
      if move_pos !=nil
        temp = @board.fields.last.get(move_pos.x,move_pos.y)
        pos = @active_piece.position
        @board.fields.last.set(move_pos.x,move_pos.y,@active_piece)
        @board.fields.last.set(pos.x,pos.y,temp)

      else
        @board.selected = Point.new(x,y)
      end
    end



    # is the position empty
    def field_empty?(coords,field)
      if field.get(coords.x,coords.y) == nil
        return true
      elsif field.get(coords.x,coords.y).owner == Player::NONE
        return true
      end
        return  false
    end


    # Get the owner of the selected field
    def get_selected_value
      if @board.selected !=nil &&  @board.fields.last.get(@board.selected.x,@board.selected.y) != nil
        return @board.fields.last.get(@board.selected.x,@board.selected.y).owner
      end
      return nil
    end

    #return hash of owners and coordinates of adjacent fields if the field is in game area
    # filter - an array of symbols :left_up, :left_down , :right_up , :right_down if nil  no filter is used
    def adjacent_coords(point,field,filter = nil)
      data = Hash.new

      if filter.nil? ||( !filter.nil? &&  filter.include?(:left_up))
        if (point.x >0) && (point.y >0)
          p = Point.new(point.x-1,point.y-1)
          data_set = {owner: field.get(p.x,p.y).owner,position: p}
          data[:left_up] = data_set
        end
      end

      if filter.nil? || ( !filter.nil? &&  filter.include?(:right_up))
        if(point.x < 7) && (point.y >0)
          p = Point.new(point.x+1,point.y-1)
          data_set = {owner: field.get(p.x,p.y).owner,position: p}
          data[:right_up] = data_set
        end
      end

      if filter.nil? || ( !filter.nil? &&  filter.include?(:left_down))
        if(point.x >0) && (point.y <7)
          p = Point.new(point.x-1,point.y+1)
          data_set = {owner: field.get(p.x,p.y).owner,position: p}
          data[:left_down] = data_set
        end
      end

      if filter.nil? || ( !filter.nil? &&  filter.include?(:right_down))
        if (point.x <7) && (point.y < 7)
          p  = Point.new(point.x+1,point.y+1)
          data_set = {owner: field.get(p.x,p.y).owner,position: p}
          data[:right_down] = data_set
        end
      end


      return data
    end


    # Returns list of empty adjacent fields and sets the available_value variable of field to those values
    def adjacent_empty(position,field,return_only_points = true,set_available_moves = true)
      return nil if field.get(position.x,position.y) == nil #if the field is not a game field no need to calculate anything
      owner = field.get(position.x,position.y).owner #get the owner of current field
      return nil if owner == Player::NONE # If the field is empty - skip calculation
      empty_points = []

      #If the piece is of rank MEN it cant move backwards so filter out unnecessary directions
      filter = []
      if @active_piece.rank == Piece::RANK[:MEN]
        if @active_piece.owner == Player::PLAYER1
          filter << :left_up << :right_up
        elsif @active_piece.owner == Player::PLAYER2
          filter << :left_down << :right_down
        end
      end

      if filter.length  <1
        filter = nil
      end

      #Get Adjacent  coordinates
      data = adjacent_coords(position,field,filter)


      # Select just coordinates of fields that are empty
      data.each_value.select do |value|
        value.each do |k,v|
          if k==:owner && v== Player::NONE
            empty_points.push value[:position]
          end
        end
      end

      #send data to available_moves variable of the field (if necessary)
      field.available_moves = empty_points if set_available_moves
      puts data
      puts "----------------"
      return empty_points if return_only_points

      data = data.select do |key,value|

      end
    end

    #Checks for empty fields along diagonals based on given data ( data returned from adjacent_empty )
    def available_moves_for_king(data)

    end


  end
end