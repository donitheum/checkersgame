module CheckersGame
  class PositionNames
    #Returns field name from coordinates
    def self.get(x,y)
      letters = ("A".."H").to_a;
      return (letters[x]+(8-y).to_s).intern
    end

    #Returns coordinates (Point object) from field name
    def self.to_coords(field_name)
        field_name = field_name.to_s.split('')
        letter = field_name.first
        digit = field_name.last.to_i

        if !((letter.between?("A","H")) || (digit.between?(1,8) ))
            raise ArgumentError , "Frong field name given should be between A0 and H8"
        end
        Point.new letter.ord - 65, 8-digit

    end
  end
end