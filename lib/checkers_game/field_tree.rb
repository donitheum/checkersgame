#Tree structure for calculating moves
class FieldTree

  #Node element
  class FieldNode
    attr_accessor :root,:value, :left,:right,:depth
    def initialize(root,value)
      @root = root
      @value = value
      if @root != nil
         @depth = @root.depth + 1
      else
        @depth = 0
      end
    end
  end


  #Tree initialization
  attr_reader :root,:current_pos
  def initialize(root)
    @root = FieldNode.new(nil,root)
    @current_pos = @root
  end

  def add_left(node)
    @current_pos.left = FieldNode.new(@current_pos,node)
    @current_pos = @current_pos.left
  end

  def add_right(node)
    @current_pos.right = FieldNode.new(@current_pos,node)
    @current_pos = @current_pos.right
  end

  def move_up
    @current_pos = @current_pos.root
  end

  def has_left?
    @current_pos.left != nil
  end

  def has_right?
    @current_pos.right !=nil
  end

  def move_left
    @current_pos = @current_pos.left
  end

  def move_right
    @current_pos = @current_pos.right
  end

  def get_depth
    return @current_pos.depth
  end

  def get_current_value
    return @current_pos.value if @current_pos != nil
    return nil
  end

  def move_to_root
    @current_pos = @root
  end
end