require_relative 'lib/checkers_game/Board'
require_relative 'lib/checkers_game/Game'
require_relative 'lib/checkers_game/Piece'
require_relative 'lib/checkers_game/Player'
require_relative 'lib/checkers_game/Point'
require_relative 'lib/checkers_game/Field'
require_relative 'lib/checkers_game/Logic'
require_relative 'lib/checkers_game/position_names'
module CheckersGame
  CheckersGame.constants.select {|c| puts  CheckersGame.const_get(c)}
  $game = CheckersGame::Game.new
  $game.set_up!
  $game.show
end
