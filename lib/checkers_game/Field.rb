module CheckersGame
  # Holds the field configuration
  class Field
    # @field - the current configuration of checkers on the board
    # @player - the player whose turn it is to make the move
    attr_accessor :field,:player,:available_moves



    def initialize(field = Array.new(8){Array.new(8)})
      @field = field
      @available_moves = nil
    end

    # Get the field at row, col returns instance of Piece or nil if the field is not a game area
    def get(row,col)
      return nil if row > 7 || col > 7
      @field[row][col]
    end

    #Set the field at row,col to the value
    def set(row,col,value)
      @field[row][col] = value
    end


    def draw
      for i in 0..7
        for j in 0..7
          @field[i][j].draw(Point.new(i,j)) if ((@field[i][j].instance_of? Piece) && @field[i][j].owner != Player::NONE)
        end
      end
    end
  end
end