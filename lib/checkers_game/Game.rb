require 'gosu'


# Game class holds all the locations to assets in hashes
# Initializes the game window, runs the Board.draw
module CheckersGame

  class Game < Gosu::Window
    attr_reader :font, :images

    SCREEN_WIDTH = 760
    SCREEN_HEIGHT = 680
    BOARD_TOP_POS_X = SCREEN_WIDTH/2-Board::BOARD_SIZE/2
    BOARD_TOP_POS_Y = SCREEN_HEIGHT/2-Board::BOARD_SIZE/2


    FONT_ASSETS = {
        font: "assets/fonts/Roboto-Black.ttf"
    }

    IMAGE_ASSETS = {
        white_piece: "assets/images/white.png",
        black_piece: "assets/images/black.png"
    }


    def initialize
      super SCREEN_WIDTH,SCREEN_HEIGHT
      self.caption = "Checkers Game"
      @images = {}
      load_assets

    end

    # $game variable has not been initialized yet in the constructor but other objects rely on it.
    # So we initialize objects in the set_up! method which is run in the GameApp file right after Game instance is created
    def set_up!
      @board = Board.new(BOARD_TOP_POS_X,BOARD_TOP_POS_Y)
    end

    def update

    end

    def draw
      draw_background
      @board.draw
    end

    def draw_background
      Gosu.draw_rect 0,0,SCREEN_WIDTH,SCREEN_HEIGHT, Gosu::Color.argb(0xff_ffffff)
    end

    def needs_cursor?
      true
    end


    def button_down(key)
      @board.button_down(key,mouse_x,mouse_y)
    end

    def load_assets
      IMAGE_ASSETS.each do |key,value|
        @images[key] = Gosu::Image.new(value)
        puts value
      end
      @font = Gosu::Font.new(self,FONT_ASSETS[:font],32)
    end
  end
end