# The piece class repesents a single checker
# The pieces position actually never changes, only the owner is changed
module CheckersGame
  class Piece
    # The rank of the piece
    RANK = {MEN: :men, KING: :king}

    # @image - instance of Gosu::Image
    # @Owner - the owner of the field (Player::Player1 , Player::Player2 , ( Player::None - if the field is not occupied)
    attr_reader :image,:owner,:position

    #@position - instance of Point class
    # @rank - value of RANK hash
    attr_accessor :rank

    def initialize(owner)
      # Check if owner is a valid value
      raise ArgumentError , "Argument 'owner' is not of Player type" unless Player.is_valid_enum? owner

      @owner = owner
      reset_owner
    end


    def reset_owner
      if owner != Player::NONE
        @image = (owner == Player::PLAYER1 ? $game.images[:white_piece] : $game.images[:black_piece])
      else
        @image = nil
      end
      @rank = RANK[:MEN]
    end

    def owner=(owner)
      @owner = owner
      # reset_owner
    end


    # Draw the piece Goku::image.draw(...)
    def draw(position)
      @position = position
      @image.draw(Game::BOARD_TOP_POS_X+position.x*Board::FIELD_SIZE+$game.images[:black_piece].width*0.8 / 8,
                  Game::BOARD_TOP_POS_Y+position.y*Board::FIELD_SIZE+$game.images[:black_piece].width*0.8 / 8,5,0.8,0.8)
    end
  end
end