# Player class represents a player
module CheckersGame
  class Player
    PLAYER1 = :player1
    PLAYER2 = :player2
    NONE = :none



    def self.is_valid_enum?(attr)
      false
      Player.constants.each do |c|
       true if c.equal? attr
      end
    end
  end
end