# Board class is initialized from the Game most of the other classes are initialized in here

module CheckersGame

  class Board
    # FIELD_SIZE - length of the side of the field square
    # BOARD_SIZE - total length of the side of the board square
    # @top_pos_x  and @top_pos_y  - the  top left position of the board on the game screen
    # @fields - an array of Field class contaning all the checker configurations on field throughout the game
    FIELD_SIZE = 64
    BOARD_SIZE = 8 * FIELD_SIZE
    attr_accessor :top_pos_y, :top_pos_x, :fields,:selected

    def initialize(top_pos_x,top_pos_y)
      @logic = Logic.new self
      @top_pos_x = top_pos_x
      @top_pos_y = top_pos_y
      @fields = []
      @selected = nil
      # Initialize the field and set up the pieces
      init_field = Field.new
      for i in 7.downto(0)
        for j in 7.downto(0)
         if ((i%2 ==1 && j%2 ==0) || (i%2 ==0 &&j%2 ==1)) && j<3
            init_field.set i,j, Piece.new(Player::PLAYER2)
          elsif ((i%2 ==1 && j%2 ==0) || (i%2 ==0 &&j%2 ==1)) && j>4
            init_field.set i,j, Piece.new(Player::PLAYER1)
          elsif ((i%2 ==1 && j%2 ==0) || (i%2 ==0 &&j%2 ==1))
            init_field.set i,j, Piece.new(Player::NONE)
          else
            init_field.set i,j ,nil
          end
        end
      end
      @fields << init_field
    end # def initialize



    # Draw the board and everything else related to gameplay
    # Everything drawn in this method gets drawn on the game screen
    def draw
      draw_board
      #@fields.each { |f| f.draw}
      @fields.last.draw();
    end # def draw

    def button_down(key,mouse_x,mouse_y)
      case key
      when Gosu::MsLeft
        @logic.process_keypress(mouse_x, mouse_y)
      end
    end

    #-------------------------------------------------------------------------------------------
    #------------------------------PRIVATE DECLARATIONS-----------------------------------------
    #-------------------------------------------------------------------------------------------

    private

    #----------Function for drawing an outline of a rectangle----------------
    def draw_rect_no_fill(x,y,width,height,color,args  = {})
      args[:line_weight] ||= 1
      args[:extend_outward] = true if args[:extend_outward].nil? #The direction in which the line thickens
      i = 0;
      args[:line_weight].times do
        #-----Top Line-----
        Gosu::draw_line x , y+i ,color , x+width ,y+i,color
        #----Bottom Line----
        Gosu::draw_line x , y+height-i ,color , x+width ,y+height-i,color
        #----Left Line-----
        Gosu::draw_line x+i , y ,color , x+i ,y+height,color
        #----Right Line----
        Gosu::draw_line x+width-i , y ,color , x+width-i ,y+height,color
        i += args[:extend_outward] ? 1 : -1
      end
    end # def draw_rect_no_fill


    def draw_selection
      return nil if @selected == nil
      return nil if @logic.field_empty?(@selected,@fields.last)
      draw_rect_no_fill  @top_pos_x + @selected.x * FIELD_SIZE ,@top_pos_y + @selected.y * FIELD_SIZE,
                      FIELD_SIZE,FIELD_SIZE,
                       Gosu::Color::WHITE,{line_weight: 3}
    end


    def draw_available_moves
      return nil if @selected == nil
      points = @logic.adjacent_empty(@selected,@fields.last)
      return nil if points == nil
      points.each do |point|
        Gosu::draw_rect(top_pos_x+10+(point.x*FIELD_SIZE),top_pos_y+10 + point.y*FIELD_SIZE, FIELD_SIZE-20,FIELD_SIZE-20,
                        Gosu::Color.argb(0xff_75f987))
      end

    end

    # Draw the board and the grid
    def draw_board
      #Board area with border
      Gosu::draw_rect top_pos_x, top_pos_y ,BOARD_SIZE ,BOARD_SIZE ,Gosu::Color.argb(0xff_ff9a0c)
      draw_rect_no_fill top_pos_x, top_pos_y , BOARD_SIZE+1,BOARD_SIZE+1,
                        Gosu::Color.argb(0xff_000000), line_weight: 1



      #The grid (fields) of the board and its borders
      for i in 0...8
        for j in 0...8
          Gosu::draw_rect(top_pos_x+i*FIELD_SIZE,top_pos_y+j*FIELD_SIZE, FIELD_SIZE,FIELD_SIZE,
                          Gosu::Color.argb(0xff_f21313)) if (i%2 == 1 && j %2 ==0) || (i%2 ==0 && j %2 ==1)
          Gosu::draw_line(top_pos_x,top_pos_y+j*FIELD_SIZE,Gosu::Color::BLACK,
                          top_pos_x+BOARD_SIZE,top_pos_y+j*FIELD_SIZE,Gosu::Color::BLACK) if i==7
          Gosu::draw_line(top_pos_x+j*FIELD_SIZE,top_pos_y,Gosu::Color::BLACK,
                          top_pos_x+j*FIELD_SIZE,top_pos_y+BOARD_SIZE,Gosu::Color::BLACK) if i == 7
        end
        draw_selection
        draw_available_moves
      end

      # Field positions markup
      letters = ("A".."H").to_a
      numbers = 8.downto(1).to_a
      letters_x_origin = @top_pos_x + FIELD_SIZE / 2 - 10
      numbers_y_origin = @top_pos_y + FIELD_SIZE / 2 - 15
      for i in 0..7  do
        $game.font.draw_text(letters[i],letters_x_origin + i* FIELD_SIZE,
                                     @top_pos_y - FIELD_SIZE/2 -5, 3, 2, 1, Gosu::Color::BLACK)
        $game.font.draw_text(numbers[i],@top_pos_x - FIELD_SIZE / 2 - 5 ,
                                     numbers_y_origin + i* FIELD_SIZE, 3, 2, 1, Gosu::Color::BLACK)
      end
    end # def draw_board

  end



end