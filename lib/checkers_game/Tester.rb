require_relative "field_tree"
class Numb
  attr_accessor :value
  def initialize(value)
    @value = value
  end

  def to_s
    value
  end
end
tree = FieldTree.new(Numb.new(5))
tree.add_left(Numb.new(10))
tree.add_left(Numb.new(15))
puts "data %d and depth %d" % [tree.get_current_value.to_s,tree.get_depth]
tree.move_up
puts tree.get_current_value.to_s
tree.move_up
puts tree.get_current_value.to_s