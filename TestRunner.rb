require 'rspec'

#Include all the files from "lib/checkers_game/"
Dir[File.join(Dir.pwd,"lib","checkers_game","*.rb")].each do |file|
  require file

end

#Include all the tests from "tests"
include CheckersGame
Dir[File.join(File.dirname(__FILE__),"tests","*.rb")].each do |file|
  require file
end


